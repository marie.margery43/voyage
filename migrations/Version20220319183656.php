<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220319183656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_travel (user_id INT NOT NULL, travel_id INT NOT NULL, INDEX IDX_485970F3A76ED395 (user_id), INDEX IDX_485970F3ECAB15B3 (travel_id), PRIMARY KEY(user_id, travel_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_travel ADD CONSTRAINT FK_485970F3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_travel ADD CONSTRAINT FK_485970F3ECAB15B3 FOREIGN KEY (travel_id) REFERENCES travel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE travel_destination DROP FOREIGN KEY FK_21450825D7E819E1');
        $this->addSql('DROP INDEX IDX_21450825D7E819E1 ON travel_destination');
        $this->addSql('ALTER TABLE travel_destination CHANGE travel_id_id travel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_destination ADD CONSTRAINT FK_21450825ECAB15B3 FOREIGN KEY (travel_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_21450825ECAB15B3 ON travel_destination (travel_id)');
        $this->addSql('ALTER TABLE travel_site DROP FOREIGN KEY FK_A21BEA492E393FE4');
        $this->addSql('DROP INDEX IDX_A21BEA492E393FE4 ON travel_site');
        $this->addSql('ALTER TABLE travel_site CHANGE destination_id_id travel_destination_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_site ADD CONSTRAINT FK_A21BEA4996A87E0D FOREIGN KEY (travel_destination_id) REFERENCES travel_destination (id)');
        $this->addSql('CREATE INDEX IDX_A21BEA4996A87E0D ON travel_site (travel_destination_id)');
        $this->addSql('ALTER TABLE user DROP relation');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_travel');
        $this->addSql('ALTER TABLE travel CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE travel_destination DROP FOREIGN KEY FK_21450825ECAB15B3');
        $this->addSql('DROP INDEX IDX_21450825ECAB15B3 ON travel_destination');
        $this->addSql('ALTER TABLE travel_destination CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE travel_id travel_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_destination ADD CONSTRAINT FK_21450825D7E819E1 FOREIGN KEY (travel_id_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_21450825D7E819E1 ON travel_destination (travel_id_id)');
        $this->addSql('ALTER TABLE travel_site DROP FOREIGN KEY FK_A21BEA4996A87E0D');
        $this->addSql('DROP INDEX IDX_A21BEA4996A87E0D ON travel_site');
        $this->addSql('ALTER TABLE travel_site CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE travel_destination_id destination_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_site ADD CONSTRAINT FK_A21BEA492E393FE4 FOREIGN KEY (destination_id_id) REFERENCES travel_destination (id)');
        $this->addSql('CREATE INDEX IDX_A21BEA492E393FE4 ON travel_site (destination_id_id)');
        $this->addSql('ALTER TABLE user ADD relation VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE roles roles LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\', CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
