<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220314222759 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE travel_destination ADD travel_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_destination ADD CONSTRAINT FK_21450825D7E819E1 FOREIGN KEY (travel_id_id) REFERENCES travel (id)');
        $this->addSql('CREATE INDEX IDX_21450825D7E819E1 ON travel_destination (travel_id_id)');
        $this->addSql('ALTER TABLE travel_site ADD destination_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE travel_site ADD CONSTRAINT FK_A21BEA492E393FE4 FOREIGN KEY (destination_id_id) REFERENCES travel_destination (id)');
        $this->addSql('CREATE INDEX IDX_A21BEA492E393FE4 ON travel_site (destination_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE travel CHANGE title title VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE travel_destination DROP FOREIGN KEY FK_21450825D7E819E1');
        $this->addSql('DROP INDEX IDX_21450825D7E819E1 ON travel_destination');
        $this->addSql('ALTER TABLE travel_destination DROP travel_id_id, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE travel_site DROP FOREIGN KEY FK_A21BEA492E393FE4');
        $this->addSql('DROP INDEX IDX_A21BEA492E393FE4 ON travel_site');
        $this->addSql('ALTER TABLE travel_site DROP destination_id_id, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE firstname firstname VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE lastname lastname VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE username username VARCHAR(50) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE address address VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE postal_code postal_code VARCHAR(10) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE city city VARCHAR(100) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(22) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
