<?php

namespace App\Controller;

use App\Entity\TravelDestination;
use App\Entity\TravelSite;
use App\Repository\TravelDestinationRepository;
use App\Repository\TravelSiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FranceController extends AbstractController
{
    /**
     * @Route("/france", name="france", methods={"GET"})
     */
    public function index(Request $request, TravelDestinationRepository $travelDestinationRepository): Response
    {
        $travelSite =$this->getDoctrine()->getRepository(TravelSite::class);
        $currenttravelSites = $request->get('travelSite') ?? null;
        $currenttravelDestination = $request->get('travelDestination') ?? null;
        
        return $this->render('france/index.html.twig', [
            'travelDestinations' =>$travelDestinationRepository->findbyEntity($request),
            'travelDesti' => $travelDestinationRepository->findAll(),
            'travelSit' =>$travelSite->findAll(),
            'currenttravelSites' => $currenttravelSites,
            'currenttravelDestination' => $currenttravelDestination,

        ]);
    }
}
