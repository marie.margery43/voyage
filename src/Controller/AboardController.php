<?php

namespace App\Controller;

use App\Entity\TravelDestination;
use App\Entity\TravelSite;
use App\Repository\TravelDestinationRepository;
use App\Repository\TravelSiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboardController extends AbstractController
{
    /**
     * @Route("/aboard", name="aboard",methods={"GET"})
     */
    public function index(Request $request, travelSiteRepository $travelSiteRepository): Response
    {
        $travelDestination = $this->getDoctrine()->getRepository(travelDestination::class);
        $currenttravelDestinationId = $request->get('travelDestination') ?? null;
         $travelSite = $this->getDoctrine()->getRepository(travelSite::class);
        $currenttravelSiteId = $request->get('travelSite') ?? null;
        
        return $this->render('aboard/index.html.twig', [
            'travelSites' => $travelSiteRepository->findByEntity($request),
            'travelSiteSelect' => $travelSiteRepository->findAll(),
            'travelDestinations' =>$travelDestination->findAll(),
            'currenttravelDestinationId' => $currenttravelDestinationId,
            'currenttravelSiteId' => $currenttravelSiteId,
        ]);
    }
}
