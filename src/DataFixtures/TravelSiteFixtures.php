<?php

namespace App\DataFixtures;

use App\Entity\TravelSite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TravelSiteFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($count = 0; $count < 5; $count++){

        $travel = new TravelSite();
        $travel->setName("voyage" . $count);
        $travel->setDescription("Voyage-Fixture" . $count);

        $manager->persist($travel);
        }
        $manager->flush();
    }
}
