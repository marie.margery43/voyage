<?php

namespace App\DataFixtures;

use App\Entity\TravelDestination;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class TravelDestinationFixtures extends Fixture implements FixtureGroupInterface
{
    public const destination_TROISIEME = 'destination-troisieme';
    public const destination_SECONDE = 'destination-seconde';
    public const destination_PREMIERE = 'destination-premiere';
    public const destination_TERMINALE = 'destination-terminale';

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $destination = new TravelDestination();
        $destination->setName('Terminale');
        $destination->setDescription('Les cours de terminale');

        $manager->persist($destination);

        $this->addReference(self::destination_TERMINALE, $destination);

        $destination = new TravelDestination();
        $destination->setName('Première');
        $destination->setDescription('Les cours de première');

        $manager->persist($destination);

        $this->addReference(self::destination_PREMIERE, $destination);

        $destination = new TravelDestination();
        $destination->setName('Seconde');
        $destination->setDescription('Les cours de seconde');

        $manager->persist($destination);

        $this->addReference(self::destination_SECONDE, $destination);

        $destination = new TravelDestination();
        $destination->setName('Troisième');
        $destination->setDescription('Les cours de troisième');

        $manager->persist($destination);

        $this->addReference(self::destination_TROISIEME, $destination);

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['courses'];
    }
}
