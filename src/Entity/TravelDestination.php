<?php

namespace App\Entity;

use App\Repository\TravelDestinationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TravelDestinationRepository::class)
 */
class TravelDestination
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Travel::class, inversedBy="travelDestinations")
     */
    private $travel;

    /**
     * @ORM\OneToMany(targetEntity=TravelSite::class, mappedBy="travelDestination")
     */
    private $travelSites;

    public function __construct()
    {
        $this->travelSites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTravel(): ?Travel
    {
        return $this->travel;
    }

    public function setTravel(?Travel $travel): self
    {
        $this->travel = $travel;

        return $this;
    }

    /**
     * @return Collection|TravelSite[]
     */
    public function getTravelSites(): Collection
    {
        return $this->travelSites;
    }

    public function addTravelSites(TravelSite $travelSites): self
    {
        if (!$this->travelSites->contains($travelSites)) {
            $this->travelSites[] = $travelSites;
        }

        return $this;
    }

    public function removeTravelSites(TravelSite $travelSites): self
    {
        if ($this->travelSites->removeElement($travelSites)) {
        }

        return $this->travelSites;
    }
}
