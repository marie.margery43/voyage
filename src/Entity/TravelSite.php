<?php

namespace App\Entity;

use App\Repository\TravelSiteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TravelSiteRepository::class)
 */
class TravelSite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=TravelDestination::class, inversedBy="travelSites")
     */
    private $travelDestination;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTravelDestination(): ?TravelDestination
    {
        return $this->travelDestination;
    }

    public function setTravelDestination(?TravelDestination $travelDestination): self
    {
        $this->travelDestination = $travelDestination;

        return $this;
    }
}
