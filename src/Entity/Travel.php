<?php

namespace App\Entity;

use App\Repository\TravelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TravelRepository::class)
 */
class Travel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;
    
    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="travels")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=TravelDestination::class, mappedBy="travel")
     */
    private $travelDestinations;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->travelDestinations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addTravel($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeTravel($this);
        }

        return $this;
    }

    /**
     * @return Collection|TravelDestination[]
     */
    public function getTravelDestinations(): Collection
    {
        return $this->travelDestinations;
    }

    public function addTravelDestination(TravelDestination $travelDestination): self
    {
        if (!$this->travelDestinations->contains($travelDestination)) {
            $this->travelDestinations[] = $travelDestination;
            $travelDestination->setTravel($this);
        }

        return $this;
    }

    public function removeTravelDestination(TravelDestination $travelDestination): self
    {
        if ($this->travelDestinations->removeElement($travelDestination)) {
            // set the owning side to null (unless already changed)
            if ($travelDestination->getTravel() === $this) {
                $travelDestination->setTravel(null);
            }
        }

        return $this;
    }
}
