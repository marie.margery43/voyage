<?php

namespace App\Repository;

use App\Entity\TravelDestination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TravelDestination|null find($id, $lockMode = null, $lockVersion = null)
 * @method TravelDestination|null findOneBy(array $criteria, array $orderBy = null)
 * @method TravelDestination[]    findAll()
 * @method TravelDestination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TravelDestinationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TravelDestination::class);
    }

    // /**
    //  * @return TravelDestination[] Returns an array of TravelDestination objects
    //  */
    
    public function findbyEntity($request)
{
    $travelSite = $request->query->get("travelSite");
    $travelDestination = $request->query->get("travelDestination");

       if ((isset($travelSite))&&(!empty($travelSite))){            
        $queryBuiler = $this->createQueryBuilder('p')                                                                 
        ->innerJoin('p.travelSites', 'travelSite') ->where('travelSite.id = :travelSite')
        ->setParameter('travelSite', $travelSite);

}elseif (isset($travelDestination)&& !empty($travelDestination)){
        $queryBuiler = $this->createQueryBuilder('p')
        ->where('p.id = :travelDestination')
        ->setParameter('travelDestination', $travelDestination);

}elseif (isset($travelDestination , $travelSite)&& !empty($travelDestination && $travelSite)){
        $queryBuiler = $this->createQueryBuilder('p')
        ->innerJoin('p.travelSites', 'travelSite')
        ->where('travelSite.id = :travelSite')
        ->setParameter('travelSite', $travelSite)
        ->andWhere('p.id = :travelDestination')
        ->setParameter('travelDestination', $travelDestination);
}else{
        $queryBuiler = $this->createQueryBuilder('p');

};

    return $queryBuiler->getQuery()->getResult();
}
    

    /*
    public function findOneBySomeField($value): ?TravelDestination
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

