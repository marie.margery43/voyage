function isInViewport(e) {
    const distance = e.getBoundingClientRect();
    return ((distance.top < 650 && distance.top > 0) || (distance.bottom > 100 && distance.bottom < 300));
}

const comments = document.querySelectorAll("#comments");
window.addEventListener('scroll', () => {
    if(comments.length > 0) {
        comments.forEach((comment) => {
            if (!comment.classList.contains('active') && isInViewport(comment)) {
                comment.classList.add('active');
                comment.getElementsByTagName("*").forEach((img) => img.classList.add("active"));
            }
        });
    }
});