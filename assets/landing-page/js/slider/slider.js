const Swiper = require('../vendor/swiper.min');

const cards = document.querySelectorAll('.card-slider');
const swipers = document.querySelectorAll('.swiper-container');

let cardSliders = [];
cards.forEach((card, i) => {
    cardSliders[i] = new Swiper(card, {
        autoplay: {
            delay: 4000,
            disableOnInteraction: false
        },
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        slidesPerView: 3,
        spaceBetween: 70,
        breakpoints: {
            // when window is <= 991px
            991: {
                slidesPerView: 1
            },
            // when window is <= 1414px
            1414: {
                slidesPerView: 2,
                spaceBetween: 40
            }
        }
    });

    swipers[i].addEventListener('mouseenter', () => {
        cardSliders[i].autoplay.stop();
    });

    swipers[i].addEventListener('mouseleave', () => {
        cardSliders[i].autoplay.stop();
    });
});