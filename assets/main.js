// CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './landing-page/sass/main.scss';

// JS
const $ = require('jquery/dist/jquery.min');
const jQuery = $;
import 'bootstrap';
import './landing-page/js/app.js';